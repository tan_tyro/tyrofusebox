<cfset delete_employee="#application.employeesmanager.DeleteEmployee(url.id)#" />

<!---<cfdump var="#delete_employee#"/>--->

<cfset response = structNew() />
<cfset response['success'] = false />

<cfif structKeyExists(delete_employee, 'recordCount') AND delete_employee.recordCount EQ 1>
    <cfset response['success'] = true />
</cfif>

<cfset page_content = response />
