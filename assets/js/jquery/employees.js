(function($) {
    $.employees = {
//    form submit
        formSubmit: function() {
            $.ajax({
                url         : '/coldfusion-fusebox5-test/index.cfm?action=employees.save',
                type        : 'POST',
                dataType    : 'json',
                data        : $('#save-employee').serialize(),
                success     : function(response) {
                    if (response.success) {
                        alert('Success.');
                    } else {
                        alert('Failed.');
                    }
                }
            });
        },

        uploadImageFile: function() {
            var formData = new FormData($("#upload-file")[0]);
            
            $.ajax({
                url             : '/coldfusion-fusebox5-test/index.cfm?action=employees.upload_file',
                type            : 'POST',
                data            : formData,
                async           : false,
                cache           : false,
                contentType     : false,
                processData     : false,
                dataType        : 'json',
                success         : function(response) {
                    if (response.success) {
                        var filename = response.filename;
                        $.employees.setEmployeeImage(filename);
                    } else {
                        alert('Failed.');
                    }
                }
            });
        },

        setEmployeeImage: function (filename) {
            var id = $("#id").val();
        
            $.ajax({
                url         : '/coldfusion-fusebox5-test/index.cfm?action=employees.set_employee_image',
                type        : 'POST',
                dataType    : 'json',
                data        : {
                    id       : id,
                    filename : filename
                },
                success     : function(response) {
                    if (response.success) {
                        alert('Image already uploaded and set as employee image.');
                    } else {
                        alert('Failed.');
                    }
                }
            });
        },

        reloadSummaryView: function() {
            var me = this;

            if ($("#search-category").val() != "") {
                me.searchRecord($("#search-category").val(), $("#search-keyword").val())
            } else {
                $.get('/coldfusion-fusebox5-test/index.cfm?action=employees.reload_summary_view', function(data) {
                var jsonData = JSON.parse(data);
                var html = "";

                for (var key in jsonData) {
                    var ID = jsonData[key].ID;
                    var DEPARTMENT_ID = jsonData[key].DEPARTMENT_ID;
                    var FULL_NAME = jsonData[key].FULL_NAME;

                    var edit_employee_cls = "edit-employee";
                    var edit_url = "/coldfusion-fusebox5-test/index.cfm?action=employees.save_form&id="+ID;
                    var delete_employee_cls = "delete-employee";
                    var delete_url = "/coldfusion-fusebox5-test/index.cfm?action=employees.delete&id="+ID;
                    var view_employee_cls = "view-employee";
                    var view_url = "/coldfusion-fusebox5-test/index.cfm?action=employees.view&id="+ID;

                    html += "<tr>";
                    html += "<td class='test'>"+ID+"</td>";
                    html += "<td>"+get_department_list[DEPARTMENT_ID]+"</td>";
                    html += "<td>"+FULL_NAME+"</td>";
                    html += "<td>";
                    html += "<a class='"+edit_employee_cls+"' rel='"+ID+"' href='"+edit_url+"'>Edit</a> ";
                    html += "<a class='"+delete_employee_cls+"' rel='"+ID+"' href='"+delete_url+"'>Delete</a> ";
                    html += "<a class='"+view_employee_cls+"' rel='"+ID+"' href='"+view_url+"'>Details</a> ";
                    html += "</td>";
                    html += "</tr>";
                }

                $('#grid-data-tbody').html(html);
            });
            }
        },

        deleteRecord: function(id) {
            $.get('/coldfusion-fusebox5-test/index.cfm?action=employees.delete', {id: id}, function(data) {
                var response = JSON.parse(data)

                if (response.success) {
                    $.employees.reloadSummaryView();
                } else {
                    alert('Failed');
                }
            });
        },

        searchRecord: function(category, keyword) {
            $.get('/coldfusion-fusebox5-test/index.cfm?action=employees.search_keyword_category', {
                category    : category, 
                keyword     : keyword
            }, function(data) {
                var jsonData = JSON.parse(data);
                var html = "";

                for (var key in jsonData) {
                    var ID = jsonData[key].ID;
                    var DEPARTMENT_ID = jsonData[key].DEPARTMENT_ID;
                    var FULL_NAME = jsonData[key].FULL_NAME;

                    var edit_employee_cls = "edit-employee";
                    var edit_url = "/coldfusion-fusebox5-test/index.cfm?action=employees.save_form&id="+ID;
                    var delete_employee_cls = "delete-employee";
                    var delete_url = "/coldfusion-fusebox5-test/index.cfm?action=employees.delete&id="+ID;
                    var view_employee_cls = "delete-employee";
                    var view_url = "/coldfusion-fusebox5-test/index.cfm?action=employees.delete&id="+ID;

                    html += "<tr>";
                    html += "<td class='test'>"+ID+"</td>";
                    html += "<td>"+get_department_list[DEPARTMENT_ID]+"</td>";
                    html += "<td>"+FULL_NAME+"</td>";
                    html += "<td>";
                    html += "<a class='"+edit_employee_cls+"' rel='"+ID+"' href='"+edit_url+"'>Edit</a> ";
                    html += "<a class='"+delete_employee_cls+"' rel='"+ID+"' href='"+delete_url+"'>Delete</a> ";
                    html += "<a class='"+view_employee_cls+"' rel='"+ID+"' href='"+view_url+"'>Details</a> ";
                    html += "</td>";
                    html += "</tr>";
                }

                $('#grid-data-tbody').html(html);
            });
        }
    };
})(jQuery);