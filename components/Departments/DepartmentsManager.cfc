<!---
  Created by Mark Dujali on 7/22/2015.
--->
<cfcomponent displayname="DepartmentsManager">
   <!--- Place your content here --->

    <cffunction name="Init" access="public" returntype="DepartmentsManager" output="false" displayname="" hint="">
        <cfreturn this />
    </cffunction>

    <cffunction name="GetDepartmentList">
        <cfquery name="results" datasource="#request.dsn#">SELECT * FROM departments</cfquery>
        <cfreturn results />
    </cffunction>

    <cffunction name="GetDepartmentByName">
        <cfargument name="name" type="string">
        <cfquery name="results" datasource="#request.dsn#">SELECT * FROM departments WHERE name='#name#'</cfquery>
        <cfreturn results />
    </cffunction>

    <cffunction name="SaveDepartment">
        <cfquery name="" datasource="#request.dsn#"></cfquery>
    </cffunction>

    <cffunction name="DeleteDepartment">
        <cfquery name="" datasource="#request.dsn#"></cfquery>
    </cffunction>

</cfcomponent>
